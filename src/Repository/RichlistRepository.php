<?php

namespace App\Repository;

use App\Entity\Richlist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Richlist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Richlist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Richlist[]    findAll()
 * @method Richlist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RichlistRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Richlist::class);
    }

    // /**
    //  * @return Richlist[] Returns an array of Richlist objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Richlist
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
