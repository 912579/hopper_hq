<?php

namespace App\EntityListener;

use App\Entity\Richlist;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

class RichlistEntityListener
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function prePersist(Richlist $item, LifecycleEventArgs $event)
    {
        $item->computeSlug($this->slugger);
    }

    public function preUpdate(Richlist $item, LifecycleEventArgs $event)
    {
        $item->computeSlug($this->slugger);
    }
}
