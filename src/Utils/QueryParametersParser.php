<?php

namespace App\Utils;

use Symfony\Component\HttpFoundation\Request;

final class QueryParametersParser
{
    public static function parse(Request $request): array
    {
        $data = [
            'page' => $request->get('page') ?? 1,
            'pageSize' => $request->get('pageSize') ?? 10
        ];

        $filter = $request->get('filter');
        if (null !== $filter) {
            $params = array_pad(explode(':', $filter, 3), 3, null);

            foreach ($params as $param) {
                if (!empty($param)) {
                    $paramHolder = [];
                    preg_match_all('/([\w]+)(\(([^\)]+)\))?/', $param, $paramHolder);
                    $pipe = array_slice(explode('|', $paramHolder[3][0], 2), 0, 2);

                    $data[$paramHolder[1][0]] = $pipe;
                }
            }
        }
        return $data;
    }
}
