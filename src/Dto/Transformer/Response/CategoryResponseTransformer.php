<?php

namespace App\Dto\Transformer\Response;

use App\Dto\CategoryOutput;
use App\Dto\Transformer\Exception\UnexpectedTypeException;
use App\Entity\Categories;

class CategoryResponseTransformer extends AbstractApiResponseTransformer
{
    /**
     * @param Categories $object
     * @return CategoryOutput
     */
    public function transformObject($object): CategoryOutput
    {
        if (!$object instanceof Categories) {
            throw new UnexpectedTypeException('Expected type of Categories but got ' . \get_class($object));
        }

        $dto = new CategoryOutput();
        $dto->id = $object->getId();
        $dto->name = $object->getName();
        $dto->code = $object->getCode();

        return $dto;
    }
}
