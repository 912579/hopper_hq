<?php

namespace App\Dto\Transformer\Response;


use App\Dto\CountryOutput;
use App\Dto\Transformer\Exception\UnexpectedTypeException;
use App\Entity\Countries;

class CountryResponseTransformer extends AbstractApiResponseTransformer
{
    /**
     * @param Countries $object
     * @return CountryOutput
     */
    public function transformObject($object): CountryOutput
    {
        if (!$object instanceof Countries) {
            throw new UnexpectedTypeException('Expected type of Countries but got ' . \get_class($object));
        }

        $dto = new CountryOutput();
        $dto->id = $object->getId();
        $dto->name = $object->getName();
        $dto->code = $object->getCode();

        return $dto;
    }
}
