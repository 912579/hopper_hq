<?php

namespace App\Dto\Transformer\Response;

interface ApiResponseTransformerInterface
{
    public function transformObject($object);
    public function transformObjects(iterable $objects): iterable;
}
