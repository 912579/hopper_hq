<?php

namespace App\Dto\Transformer\Response;

use App\Dto\RichlistOutput;
use App\Dto\Transformer\Exception\UnexpectedTypeException;
use App\Entity\Richlist;

class RichlistResponseTransformer extends AbstractApiResponseTransformer
{
    /**
     * @var CategoryResponseTransformer
     */
    private CategoryResponseTransformer $categoryResponseTransformer;

    /**
     * @var CountryResponseTransformer
     */
    private CountryResponseTransformer $countryResponseTransformer;

    public function __construct(
        CategoryResponseTransformer $categoryResponseTransformer,
        CountryResponseTransformer $countryResponseTransformer
    ){
        $this->categoryResponseTransformer = $categoryResponseTransformer;
        $this->countryResponseTransformer = $countryResponseTransformer;
    }
    /**
     * @param Richlist $product
     *
     * @return RichlistOutput
     */
    public function transformObject($object): RichlistOutput
    {
        if (!$object instanceof Richlist) {
            throw new UnexpectedTypeException('Expected type of Richlist but got ' . \get_class($object));
        }

        $dto = new RichlistOutput();
        $dto->id = $object->getId();
        $dto->name = $object->getName();
        $dto->slug = $object->getSlug();
        $dto->username = $object->getUsername();
        $dto->category = $object->getCategory() ? $this->categoryResponseTransformer->transformObject($object->getCategory()) : null;
        $dto->country = $object->getCountry()? $this->countryResponseTransformer->transformObject($object->getCountry()) : null;
        $dto->followers = $object->getFollowers();
        $dto->cost_per_post = $object->getCostPerPost();
        $dto->createdAt = $object->getCreatedAt();

        return $dto;
    }
}
