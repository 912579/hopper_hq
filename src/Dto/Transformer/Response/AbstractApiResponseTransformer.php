<?php

namespace App\Dto\Transformer\Response;

abstract  class AbstractApiResponseTransformer implements ApiResponseTransformerInterface
{
    public function transformObjects(iterable $objects): iterable
    {
        $dto = [];

        foreach ($objects as $object) {
            $dto[] = $this->transformObject($object);
        }

        return $dto;
    }
}
