<?php

namespace App\Dto;

use JMS\Serializer\Annotation as Serialization;

class CountryOutput
{
    /**
     * @Serialization\Type("int")
     */
    public int $id;

    /**
     * @Serialization\Type("string")
     */
    public string $name;

    /**
     * @Serialization\Type("string")
     */
    public ?string $code;
}
