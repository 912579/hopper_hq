<?php

namespace App\Dto;

use JMS\Serializer\Annotation as Serialization;

class RichlistOutput
{

    /**
     * @Serialization\Type("int")
     */
    public $id;

    /**
     * @Serialization\Type("string")
     */
    public ?string $name;

    /**
     * @Serialization\Type("string")
     */
    public ?string $slug;

    /**
     * @Serialization\Type("string")
     */
    public ?string $username;

    /**
     * @Serialization\Type("App\Dto\CategoryOutput")
     */
    public CategoryOutput $category;

    /**
     * @Serialization\Type("int")
     */
    public int $followers;

    /**
     * @Serialization\Type("float<2, ?HALF_DOWN?>")
     */
    public float $cost_per_post;

    /**
     * @Serialization\Type("App\Dto\CountryOutput")
     */
    public ?CountryOutput $country;

    /**
     * @Serialization\Type("DateTime<'Y-m-d\TH:i:s'>")
     */
    public \DateTime $createdAt;
}
