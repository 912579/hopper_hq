<?php

namespace App\Service;

use App\Dto\RichlistOutput;
use App\Dto\Transformer\Response\RichlistResponseTransformer;
use App\Entity\Categories;
use App\Entity\Countries;
use App\Entity\Richlist;
use Doctrine\ORM\EntityManagerInterface;
use Maldoinc\Doctrine\Filter\DoctrineFilter;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RichlistService
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * @var TranslatorInterface
     */
    private TranslatorInterface $translate;

    /**
     * @var RichlistResponseTransformer
     */
    private RichlistResponseTransformer $richlistResponseTransformer;

    /**
     * @var array
     */
    private array $errors = [];

    /**
     * @var Richlist
     */
    private Richlist $currentItem;

    /**
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     * @param ValidatorInterface $validator
     * @param RichlistResponseTransformer $richlistResponseTransformer
     * @param SerializerInterface $serializer
     * @param RequestStack $requestStack
     */
    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        TranslatorInterface $translator,
        ValidatorInterface $validator,
        RichlistResponseTransformer $richlistResponseTransformer,
        SerializerInterface $serializer,
        RequestStack $requestStack
    ) {
        $this->em = $em;
        $this->logger = $logger;
        $this->translate = $translator;
        $this->validator = $validator;
        $this->richlistResponseTransformer = $richlistResponseTransformer;
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function createItem(Request $request): bool
    {
        try {
            $item = new Richlist();
            $item->setName($request->get('name') ?: '');
            $item->setSlug($request->get('slug') ?: '');
            $item->setUsername($request->get('username') ?: '');
            $item->setRank($request->get('rank') ?: 0);
            $item->setCostPerPost($request->get('cost_per_post') ?: 0);
            $item->setFollowers($request->get('followers') ?: 0);
            $category = $this->em->getRepository(Categories::class)->find($request->get('category_id') ?: null);
            $item->setCategory($category);
            $country = $this->em->getRepository(Countries::class)->find($request->get('country_id') ?: null);
            $item->setCountry($country);
            $violations = $this->validator->validate($item);

            if (count($violations) > 0) {
                $errorMessages = [];

                foreach ($violations as $violation) {
                    $errorMessages[$violation->getPropertyPath()] = $violation->getMessage();
                }

                $this->setErrors($errorMessages);

                return false;
            }

            $this->em->persist($item);
            $this->em->flush();

            $this->setCurrentIntem($item);
        } catch (\Exception $ex) {
            $this->setErrors([$ex->getMessage()]);
            $this->logger->error($ex->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param Richlist $item
     * @param Request $request
     * @return bool
     */
    public function updateItem(Richlist $item, Request $request): bool {
        try {
            if($name = $request->get('name')) {
                $item->setName($name);
            }
            if($slug = $request->get('slug')) {
                $item->setSlug($slug);
            }
            if($username = $request->get('username')) {
                $item->setUsername($username);
            }
            if($rank = $request->get('rank')) {
                $item->setRank($rank);
            }
            if($costPerPost = $request->get('cost_per_post')) {
                $item->setCostPerPost($costPerPost);
            }
            if($followers = $request->get('followers')) {
                $item->setFollowers($followers);
            }
            if($categoryId = $request->get('category_id')) {
                $category = $this->em->getRepository(Categories::class)->find($categoryId);
                $item->setCategory($category);
            }
            if($countryId = $request->get('country_id')) {
                $country = $this->em->getRepository(Countries::class)->find($countryId);
                $item->setCountry($country);
            }
            $violations = $this->validator->validate($item);

            if (count($violations) > 0) {
                $errorMessages = [];

                foreach ($violations as $violation) {
                    $errorMessages[$violation->getPropertyPath()] = $violation->getMessage();
                }

                $this->setErrors($errorMessages);

                return false;
            }

            $this->em->persist($item);
            $this->em->flush();

            $this->setCurrentIntem($item);
        } catch (\Exception $ex) {
            $this->setErrors([$ex->getMessage()]);
            $this->logger->error($ex->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function getItem(int $id): bool
    {
        try {
            $item = $this->em->getRepository(Richlist::class)->find($id);
            if(!$item) {
                return false;
            }
            //return $this->richlistResponseTransformer->transformObject($item);
            $this->setCurrentIntem($item);
        } catch (\Exception $ex) {
            $this->setErrors([$ex->getMessage()]);
            $this->logger->error($ex->getMessage());
            return false;
        }

    }

    /**
     * @return iterable
     */
    public function getAll(): iterable
    {
        $query = $this->requestStack->getCurrentRequest()->query;
        $queryBuilder = $this->em->getRepository(Richlist::class)->createQueryBuilder('inf');

        DoctrineFilter::applyFromArray($queryBuilder, $query->all());

        $pagerfanta = new Pagerfanta(new QueryAdapter($queryBuilder));
        $pagerfanta->setMaxPerPage($query->getInt('perPage', 10));
        $pagerfanta->setCurrentPage($query->getInt('page', 1));

        return [
            'data' => $this->richlistResponseTransformer->transformObjects($pagerfanta->getCurrentPageResults()),
            'pagination' => [
                'current_page' => $pagerfanta->getCurrentPage(),
                'total_pages' => $pagerfanta->getNbPages(),
                'per_page' => $pagerfanta->getMaxPerPage(),
                'count' => $pagerfanta->getNbResults()
            ]
        ];
    }

    /**
     * @param Richlist $item
     * @return bool
     */
    public function deleteItem(Richlist $item): bool
    {
        try {
            $this->em->remove($item);
            $this->em->flush();
        } catch (\Exception $ex) {
            $this->setErrors([$ex->getMessage()]);
            $this->logger->error($ex->getMessage());
            return false;
        }
        return true;
    }

    public function getTransformedCurrentItem(): RichlistOutput
    {
        return $this->richlistResponseTransformer->transformObject($this->getCurrentItem());
    }

    /**
     * @param $errors
     */
    private function setErrors(array $errors = []): void
    {
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param Richlist $item
     */
    private function setCurrentIntem(Richlist $item): void
    {
        $this->currentItem = $item;
    }

    /**
     * @return Richlist
     */
    public function getCurrentItem(): Richlist
    {
        return $this->currentItem;
    }

}
