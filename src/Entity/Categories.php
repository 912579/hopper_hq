<?php

namespace App\Entity;

use App\Repository\CategoriesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=CategoriesRepository::class)
 */
class Categories
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Richlist::class, mappedBy="category")
     */
    private $richlists;

    public function __construct()
    {
        $this->richlists = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Richlist[]
     */
    public function getRichlists(): Collection
    {
        return $this->richlists;
    }

    public function addRichlist(Richlist $richlist): self
    {
        if (!$this->richlists->contains($richlist)) {
            $this->richlists[] = $richlist;
            $richlist->setCategory($this);
        }

        return $this;
    }

    public function removeRichlist(Richlist $richlist): self
    {
        if ($this->richlists->removeElement($richlist)) {
            // set the owning side to null (unless already changed)
            if ($richlist->getCategory() === $this) {
                $richlist->setCategory(null);
            }
        }

        return $this;
    }
}
