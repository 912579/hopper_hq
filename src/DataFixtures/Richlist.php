<?php

namespace App\DataFixtures;

use App\Entity\Categories;
use App\Entity\Countries;
use App\Entity\Influencers;
use App\Entity\Richlist as Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Richlist extends Fixture
{
    const LIST = [
        [
            "slug" => "ariana-grande",
            "name" => "Ariana Grande",
            "username" => "arianagrande",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 240213263,
            "cost_per_post" => 1510000,
            "rank" => 3
        ],
        [
            "slug" => "beyonce-knowles",
            "name" => "Beyonce  Knowles",
            "username" => "beyonce",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 182421189,
            "cost_per_post" => 1147000,
            "rank" => 8
        ],
        [
            "slug" => "cristiano-ronaldo",
            "name" => "Cristiano Ronaldo",
            "username" => "cristiano",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "europe",
            "country_name" => "Europe",
            "followers" => 295984958,
            "cost_per_post" => 1604000,
            "rank" => 1
        ],
        [
            "slug" => "dwayne-johnson",
            "name" => "Dwayne Johnson",
            "username" => "therock",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 242182295,
            "cost_per_post" => 1523000,
            "rank" => 2
        ],
        [
            "slug" => "justin-bieber",
            "name" => "Justin Bieber",
            "username" => "justinbieber",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 176806227,
            "cost_per_post" => 1112000,
            "rank" => 9
        ],
        [
            "slug" => "kim-kardashian",
            "name" => "Kim  Kardashian",
            "username" => "kimkardashian",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 225742794,
            "cost_per_post" => 1419000,
            "rank" => 6
        ],
        [
            "slug" => "kylie-jenner",
            "name" => "Kylie Jenner",
            "username" => "kyliejenner",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 237603576,
            "cost_per_post" => 1494000,
            "rank" => 4
        ],
        [
            "slug" => "lionel-messi",
            "name" => "Lionel  Messi",
            "username" => "leomessi",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "south-america",
            "country_name" => "South America",
            "followers" => 215727437,
            "cost_per_post" => 1169000,
            "rank" => 7
        ],
        [
            "slug" => "selena-gomez",
            "name" => "Selena Gomez",
            "username" => "selenagomez",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 233494368,
            "cost_per_post" => 1468000,
            "rank" => 5
        ],
        [
            "slug" => "kendall-jenner",
            "name" => "Kendall  Jenner",
            "username" => "kendalljenner",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 167433486,
            "cost_per_post" => 1053000,
            "rank" => 10
        ],
        [
            "slug" => "taylor-swift",
            "name" => "Taylor  Swift",
            "username" => "taylorswift",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 160960397,
            "cost_per_post" => 1012000,
            "rank" => 11
        ],
        [
            "slug" => "jennifer-lopez",
            "name" => "Jennifer Lopez",
            "username" => "jlo",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 157061247,
            "cost_per_post" => 988000,
            "rank" => 12
        ],
        [
            "slug" => "khloe-kardashian",
            "name" => "Khloe Kardashian",
            "username" => "khloekardashian",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 150824894,
            "cost_per_post" => 949000,
            "rank" => 13
        ],
        [
            "slug" => "nicki-minaj",
            "name" => "Nicki  Minaj",
            "username" => "nickiminaj",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 139004559,
            "cost_per_post" => 874000,
            "rank" => 14
        ],
        [
            "slug" => "miley-cyrus",
            "name" => "Miley Cyrus",
            "username" => "mileycyrus",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 133357159,
            "cost_per_post" => 838300,
            "rank" => 15
        ],
        [
            "slug" => "neymar-da-silva-santos-junior",
            "name" => "Neymar da Silva Santos Junior",
            "username" => "neymarjr",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "south-america",
            "country_name" => "South America",
            "followers" => 151910286,
            "cost_per_post" => 824000,
            "rank" => 16
        ],
        [
            "slug" => "kourtney-kardashian",
            "name" => "Kourtney Kardashian",
            "username" => "kourtneykardash",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 124809341,
            "cost_per_post" => 785000,
            "rank" => 17
        ],
        [
            "slug" => "kevin-hart",
            "name" => "Kevin  Hart",
            "username" => "kevinhart4real",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 112302552,
            "cost_per_post" => 706000,
            "rank" => 18
        ],
        [
            "slug" => "virat-kohli",
            "name" => "Virat Kohli",
            "username" => "virat.kohli",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "asia",
            "country_name" => "Asia",
            "followers" => 125412624,
            "cost_per_post" => 680000,
            "rank" => 19
        ],
        [
            "slug" => "demi-lovato",
            "name" => "Demi  Lovato",
            "username" => "ddlovato",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 106159825,
            "cost_per_post" => 668000,
            "rank" => 20
        ],
        [
            "slug" => "robyn-rihanna-fenty",
            "name" => "Robyn Rihanna Fenty",
            "username" => "badgalriri",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 97121279,
            "cost_per_post" => 610500,
            "rank" => 21
        ],
        [
            "slug" => "belcalis-cardi-b-almanzar",
            "name" => "Belcalis &#39;Cardi B&#39; Almánzar",
            "username" => "iamcardib",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 93936553,
            "cost_per_post" => 591000,
            "rank" => 22
        ],
        [
            "slug" => "billie-eilish",
            "name" => "Billie Eilish",
            "username" => "billieeilish",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 86235815,
            "cost_per_post" => 543000,
            "rank" => 23
        ],
        [
            "slug" => "lebron-james",
            "name" => "LeBron James",
            "username" => "kingjames",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 87457374,
            "cost_per_post" => 474000,
            "rank" => 24
        ],
        [
            "slug" => "dua-lipa",
            "name" => "Dua Lipa",
            "username" => "dualipa",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "uk",
            "country_name" => "UK",
            "followers" => 67157186,
            "cost_per_post" => 422200,
            "rank" => 25
        ],
        [
            "slug" => "gigi-hadid",
            "name" => "Gigi Hadid",
            "username" => "gigihadid",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 66991712,
            "cost_per_post" => 421200,
            "rank" => 26
        ],
        [
            "slug" => "priyanka-chopra-jonas",
            "name" => "Priyanka Chopra Jonas",
            "username" => "priyankachopra",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "asia",
            "country_name" => "Asia",
            "followers" => 64025024,
            "cost_per_post" => 403000,
            "rank" => 27
        ],
        [
            "slug" => "emma-watson",
            "name" => "Emma Watson",
            "username" => "emmawatson",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "uk",
            "country_name" => "UK",
            "followers" => 59744444,
            "cost_per_post" => 375600,
            "rank" => 28
        ],
        [
            "slug" => "david-beckham",
            "name" => "David  Beckham",
            "username" => "davidbeckham",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "uk",
            "country_name" => "UK",
            "followers" => 66703827,
            "cost_per_post" => 362000,
            "rank" => 29
        ],
        [
            "slug" => "will-smith",
            "name" => "Will Smith",
            "username" => "willsmith",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 53792447,
            "cost_per_post" => 338200,
            "rank" => 30
        ],
        [
            "slug" => "ronaldo-de-assis-moreira",
            "name" => "Ronaldo de Assis Moreira",
            "username" => "ronaldinho",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "south-america",
            "country_name" => "South America",
            "followers" => 55486934,
            "cost_per_post" => 301000,
            "rank" => 31
        ],
        [
            "slug" => "millie-bobby-brown",
            "name" => "Millie Bobby-Brown",
            "username" => "milliebobbybrown",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "uk",
            "country_name" => "UK",
            "followers" => 45858372,
            "cost_per_post" => 289000,
            "rank" => 32
        ],
        [
            "slug" => "dove-cameron",
            "name" => "Dove Cameron",
            "username" => "dovecameron",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 42880912,
            "cost_per_post" => 269600,
            "rank" => 33
        ],
        [
            "slug" => "zlatan-ibrahimovic",
            "name" => "Zlatan Ibrahimovic",
            "username" => "iamzlatanibrahimovic",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "europe",
            "country_name" => "Europe",
            "followers" => 47663675,
            "cost_per_post" => 259000,
            "rank" => 34
        ],
        [
            "slug" => "gareth-bale",
            "name" => "Gareth Bale",
            "username" => "garethbale11",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "uk",
            "country_name" => "UK",
            "followers" => 43827172,
            "cost_per_post" => 238000,
            "rank" => 35
        ],
        [
            "slug" => "mohamed-salah",
            "name" => "Mohamed  Salah",
            "username" => "mosalah",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "africa",
            "country_name" => "Africa",
            "followers" => 42486945,
            "cost_per_post" => 231000,
            "rank" => 36
        ],
        [
            "slug" => "cole-sprouse",
            "name" => "Cole Sprouse",
            "username" => "colesprouse",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 35574240,
            "cost_per_post" => 223700,
            "rank" => 37
        ],
        [
            "slug" => "luis-suarez",
            "name" => "Luis Suarez",
            "username" => "luissuarez9",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "south-america",
            "country_name" => "South America",
            "followers" => 40750128,
            "cost_per_post" => 221000,
            "rank" => 38
        ],
        [
            "slug" => "chrissy-teigen",
            "name" => "Chrissy Teigen",
            "username" => "chrissyteigen",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 34959001,
            "cost_per_post" => 219800,
            "rank" => 39
        ],
        [
            "slug" => "conor-mcgregor",
            "name" => "Conor Mcgregor",
            "username" => "thenotoriousmma",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "uk",
            "country_name" => "UK",
            "followers" => 40155989,
            "cost_per_post" => 218000,
            "rank" => 40
        ],
        [
            "slug" => "shay-mitchell",
            "name" => "Shay Mitchell",
            "username" => "shaymitchell",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 30103400,
            "cost_per_post" => 189300,
            "rank" => 41
        ],
        [
            "slug" => "stephen-curry",
            "name" => "Stephen Curry",
            "username" => "stephencurry30",
            "category_code" => "sport",
            "category_name" => "Sport",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 34438457,
            "cost_per_post" => 187000,
            "rank" => 42
        ],
        [
            "slug" => "lili-reinhart",
            "name" => "Lili Reinhart",
            "username" => "lilireinhart",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 29074343,
            "cost_per_post" => 182800,
            "rank" => 43
        ],
        [
            "slug" => "eleonora-pons",
            "name" => "Eleonora Pons",
            "username" => "lelepons",
            "category_code" => "influencer",
            "category_name" => "Influencer",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 44063998,
            "cost_per_post" => 180000,
            "rank" => 44
        ],
        [
            "slug" => "charli-damelio",
            "name" => "Charli D&#39;Amelio",
            "username" => "charlidamelio",
            "category_code" => "influencer",
            "category_name" => "Influencer",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 42352928,
            "cost_per_post" => 172700,
            "rank" => 45
        ],
        [
            "slug" => "camila-mendes",
            "name" => "Camila Mendes",
            "username" => "camimendes",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 26605228,
            "cost_per_post" => 167300,
            "rank" => 46
        ],
        [
            "slug" => "sabrina-carpenter",
            "name" => "Sabrina Carpenter",
            "username" => "sabrinacarpenter",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 26127420,
            "cost_per_post" => 164300,
            "rank" => 47
        ],
        [
            "slug" => "addison-rae",
            "name" => "Addison Rae",
            "username" => "addisonraee",
            "category_code" => "influencer",
            "category_name" => "Influencer",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 38221120,
            "cost_per_post" => 155800,
            "rank" => 48
        ],
        [
            "slug" => "madelaine-petsch",
            "name" => "Madelaine Petsch",
            "username" => "madelame",
            "category_code" => "celebrity",
            "category_name" => "Celebrity",
            "country_code" => "usa",
            "country_name" => "USA",
            "followers" => 24457731,
            "cost_per_post" => 153800,
            "rank" => 49
        ],
        [
            "slug" => "caio-castro",
            "name" => "Caio Castro",
            "username" => "caiocastro",
            "category_code" => "travel",
            "category_name" => "Travel",
            "country_code" => "south-america",
            "country_name" => "South America",
            "followers" => 19105750,
            "cost_per_post" => 151000,
            "rank" => 50
        ]
    ];


    public function load(ObjectManager $manager): void
    {

        $stmt = $manager->getConnection()->prepare('SELECT * FROM categories');
        $res = $stmt->execute()->fetchAll();
        dd($res);
        foreach (self::LIST as $item) {
            $category = new Categories();
            $category->setCode($item['category_code']);
            $category->setName($item['category_name']);

            $manager->persist($category);


            $countries = new Countries();
            $countries->setCode($item['country_code']);
            $countries->setName($item['country_name']);

            $manager->persist($countries);


            $influencers = new Influencers();
            $influencers->setName($item['name']);
            $influencers->setSlug($item['slug']);
            $influencers->setUsername($item['username']);

            $manager->persist($influencers);


            $richlist = new Item();
            $richlist->setFollowers($item['followers']);
            $richlist->setCostPerPost($item['cost_per_post']);
            $richlist->setRank($item['rank']);
            $this->setReference($item['country_code'], $richlist);
            $this->setReference($item['category_code'], $richlist);
            $this->setReference($item['slug'], $richlist);


            $manager->persist($richlist);

        }

        $manager->flush();
    }
}
