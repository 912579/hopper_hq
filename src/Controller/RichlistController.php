<?php

namespace App\Controller;

use App\Repository\RichlistRepository;
use App\Service\RichlistService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class PostController
 * @package App\Controller
 * @Route("/api", name="richlist_api")
 */
class RichlistController extends ApiController implements RestfullApiInterface
{
    private $richlistRepository;
    private $serializer;
    private $richlistService;

    public function __construct(RichlistRepository $richlistRepository, SerializerInterface $serializer, RichlistService $richlistService){
        $this->richlistRepository = $richlistRepository;
        $this->serializer = $serializer;
        $this->richlistService = $richlistService;
    }

    /**
     * @return JsonResponse
     *
     * @Route("/items", name=".items", methods={"GET"})
     *
     * You can call it in this way: /api/items?page=1&perPage=10&orderBy[cost_per_post]=desc&category[eq]=4
     */
    public function getAll(Request $request): JsonResponse
    {

        $models = $this->richlistService->getAll();
        $data = $data = $this->serializer->normalize($models);
        return $this->response($data);
    }

    /**
     * @param int $id
     * @return JsonResponse
     *
     * @Route("/items/{id}", name=".item_get", methods={"GET"})
     */
    public function getItem(int $id): JsonResponse
    {
        $item = $this->richlistService->getItem($id);
        if($item) {
            $currentItem = $this->richlistService->getTransformedCurrentItem();
            $data = $this->serializer->normalize($currentItem);
            return $this->response($data);
        } else {
            $errors = $this->richlistService->getErrors();
            return $this->errorNotFound('Resource Not Found', $errors);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/items", name=".item_create", methods={"POST"})
     */
    public function create(Request $request): JsonResponse
    {
        try {
            $request = $this->transformJsonBody($request);
            $item = $this->richlistService->createItem($request);
            if(!$item) {
                return $this->respondValidationError('Validation errors', $this->richlistService->getErrors());
            }
            $createdItem = $this->richlistService->getTransformedCurrentItem();
            $data = $this->serializer->normalize($createdItem);
            return $this->respondCreated($data);
        } catch (\Exception $exception){

            return $this->respondValidationError('Validation errors', [$exception->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/items/{id}", name=".item_update", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        try {
            $request = $this->transformJsonBody($request);
            $richlistItem = $this->richlistRepository->find($id);
            if(!$richlistItem) {
                return $this->errorNotFound();
            }
            $item = $this->richlistService->updateItem($richlistItem, $request);
            if(!$item) {
                return $this->respondValidationError('Validation errors', $this->richlistService->getErrors());
            }
            $updatedItem = $this->richlistService->getTransformedCurrentItem();
            $data = $this->serializer->normalize($updatedItem);
            return $this->response($data);
        } catch (\Exception $exception){

            return $this->respondValidationError('Validation errors', [$exception->getMessage()]);
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     *
     * @Route("/items/{id}", name=".item_delete", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        try {
            $richlistItem = $this->richlistRepository->find($id);
            if(!$richlistItem) {
                return $this->errorNotFound();
            }
            $item = $this->richlistService->deleteItem($richlistItem);
            if(!$item) {
                return $this->errorInternalError('Internal Error', $this->richlistService->getErrors());
            }
            return $this->respondWithSuccess('Item was deleted successfully!');
        } catch (\Exception $exception){
            return $this->errorInternalError('Internal Error', [$exception->getMessage()]);
        }
    }

}
