<?php

namespace App\Controller;

use App\Repository\CategoriesRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class CategoriesController extends ApiController
{
    /**
     * @Route("/categories")
     */
    public function getAll(CategoriesRepository $repoCategories, SerializerInterface $serializer): JsonResponse
    {
        $models = $repoCategories->findAll();
        $data = $serializer->serialize($models, JsonEncoder::FORMAT);
        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/categories/{id}")
     */
    public function getOne($id, CategoriesRepository $repoCategories, SerializerInterface $serializer): JsonResponse
    {
        $models = $repoCategories->find($id);
        $data = $serializer->serialize($models, JsonEncoder::FORMAT);
        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}
