<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;

interface RestfullApiInterface
{
    public function getAll(Request $request);

    public function create(Request $request);

    public function getItem(int $id);

    public function update(int $id, Request $request);

    public function delete(int $id);
}
