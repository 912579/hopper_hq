<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends AbstractController
{
    /**
     * @var integer HTTP status code - 200 (OK) by default
     */
    protected $statusCode = 200;

    const CODE_WRONG_ARGS = 'GEN-FUBARGS';
    const CODE_NOT_FOUND = 'GEN-LIKETHEWIND';
    const CODE_INTERNAL_ERROR = 'GEN-AAAGGH';
    const CODE_UNAUTHORIZED = 'GEN-MAYBGTFO';
    const CODE_FORBIDDEN = 'GEN-GTFO';
    const CODE_INVALID_INPUT = 'GEN-UMWUT';

    /**
     * Gets the value of statusCode.
     *
     * @return integer
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param integer $statusCode the status code
     *
     * @return self
     */
    protected function setStatusCode(int $statusCode): ApiController
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Returns a JSON response
     *
     * @param array $data
     * @param array $headers
     *
     * @return JsonResponse
     */
    public function response($data, array $headers = []): JsonResponse
    {
        return new JsonResponse($data, $this->getStatusCode(), $headers);
    }

    /**
     * Sets an error message and returns a JSON response
     *
     * @param string $message
     * @param string $errorCode
     * @param array $errors
     * @param array $headers
     *
     * @return JsonResponse
     */
    public function respondWithErrors(string $message, string $errorCode, array $errors = [], array $headers = []): JsonResponse
    {
        $data = [
            'status' => $this->getStatusCode(),
            'errors' => $errors,
            'message' => $message,
            'code'    => $errorCode,
        ];

        return new JsonResponse($data, $this->getStatusCode(), $headers);
    }

    /**
     * Sets an error message and returns a JSON response
     *
     * @param string $success
     * @param array $headers
     *
     * @return JsonResponse
     */
    public function respondWithSuccess(string $success, array $headers = []): JsonResponse
    {
        $data = [
            'status' => $this->getStatusCode(),
            'success' => $success,
        ];

        return new JsonResponse($data, $this->getStatusCode(), $headers);
    }

    /**
     * Respond the error of 'Wrong Arguments'.
     *
     * @param string $message
     * @param array $errors
     *
     * @return JsonResponse
     */
    public function errorWrongArgs(string $message = 'Wrong Arguments', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(400)->respondWithErrors($message, self::CODE_WRONG_ARGS, $errors);
    }

    /**
     * Respond the error of 'Forbidden'
     *
     * @param string $message
     * @param array $errors
     *
     * @return JsonResponse
     */
    public function errorForbidden(string $message = 'Forbidden', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(500)->respondWithErrors($message, self::CODE_FORBIDDEN, $errors);
    }

    /**
     * Returns a 500 Internal Server Error
     *
     * @param string $message
     * @param array $errors
     *
     * @return JsonResponse
     */
    public function errorInternalError(string $message = 'Internal Error', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(500)->respondWithErrors($message, self::CODE_INTERNAL_ERROR, $errors);
    }

    /**
     * Returns a 401 Unauthorized http response
     *
     * @param string $message
     * @param array $errors
     *
     * @return JsonResponse
     */
    public function errorUnauthorized(string $message = 'Not authorized!', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(401)->respondWithErrors($message, self::CODE_UNAUTHORIZED, $errors);
    }

    /**
     * Returns a 422 Unprocessable Entity
     *
     * @param string $message
     * @param array $errors
     *
     * @return JsonResponse
     */
    public function respondValidationError(string $message = 'Validation errors', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(422)->respondWithErrors($message, self::CODE_INVALID_INPUT, $errors);
    }

    /**
     * Returns a 404 Not Found
     *
     * @param string $message
     * @param array $errors
     *
     * @return JsonResponse
     */
    public function errorNotFound(string $message = 'Resource Not Found', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(404)->respondWithErrors($message, self::CODE_NOT_FOUND, $errors);
    }

    /**
     * Returns a 201 Created
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    public function respondCreated(array $data = []): JsonResponse
    {
        return $this->setStatusCode(201)->response($data);
    }

    // this method allows us to accept JSON payloads in POST requests
    // since Symfony 4 doesn’t handle that automatically:

    protected function transformJsonBody(Request $request): Request
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }
}
