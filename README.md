###Filtering
Below is a complete list of the supported operators.

| Operator | Description | Example |
| --- | --- | --- |
| `eq` | Equality | `name[eq]=Jimothy` |
| `neq` | Inequality | `status[neq]=backlog` |
| `gt` | Greater than | `price[gt]=10` |
| `gte` | Greater than or equal | `price[gte]=10` |
| `lt` | Less than | `stock[lt]=100` |
| `lte` | Less than or equal | `stock[lte]=100` |
| `in` | In  | `id[in][]=1&id[in][]=2` |
| `not_in` | Not in | `roles[not_in][]=ROLE_ADMIN` |
| `is_null` | Is null | `subscribedAt[is_null]` |
| `is_not_null` | Is not null | `subscribedAt[is_not_null]` |
| `starts_with` | Starts with | `name[starts_with]=a` |
| `ends_with` | Ends with | `email[ends_with]=@gmail.com` |
| `contains` | Contains | `name[containts]=d` |

###Ordering
Ordering is applied via the `orderBy` query string key. Ordering is applied via the following syntax: `orderBy[fieldName]=direction` where direction can be either `asc` or `desc`.

The `orderBy` key can be used multiple times to allow sorting by multiple fields. E.g: `orderBy[id]=desc&orderBy[lastName]=asc`
