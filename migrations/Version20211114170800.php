<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211114170800 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_D7D726CE5E237E06 ON richlist');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D7D726CE989D9B62 ON richlist (slug)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_D7D726CE989D9B62 ON richlist');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D7D726CE5E237E06 ON richlist (name)');
    }
}
