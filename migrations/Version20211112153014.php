<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211112153014 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE richlist DROP FOREIGN KEY FK_D7D726CE4AF97FA6');
        $this->addSql('DROP TABLE influencers');
        $this->addSql('DROP INDEX IDX_D7D726CE4AF97FA6 ON richlist');
        $this->addSql('ALTER TABLE richlist ADD slug VARCHAR(255) NOT NULL, ADD name VARCHAR(255) NOT NULL, ADD username VARCHAR(30) NOT NULL, DROP influencer_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE influencers (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, username VARCHAR(30) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE richlist ADD influencer_id INT NOT NULL, DROP slug, DROP name, DROP username');
        $this->addSql('ALTER TABLE richlist ADD CONSTRAINT FK_D7D726CE4AF97FA6 FOREIGN KEY (influencer_id) REFERENCES influencers (id)');
        $this->addSql('CREATE INDEX IDX_D7D726CE4AF97FA6 ON richlist (influencer_id)');
    }
}
