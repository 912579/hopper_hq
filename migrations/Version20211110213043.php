<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211110213043 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE countries (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE influencers (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, username VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE richlist (id INT AUTO_INCREMENT NOT NULL, influencer_id INT NOT NULL, category_id INT NOT NULL, country_id INT NOT NULL, followers INT NOT NULL, cost_per_post DOUBLE PRECISION NOT NULL, rank INT NOT NULL, INDEX IDX_D7D726CE4AF97FA6 (influencer_id), INDEX IDX_D7D726CE12469DE2 (category_id), INDEX IDX_D7D726CEF92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE richlist ADD CONSTRAINT FK_D7D726CE4AF97FA6 FOREIGN KEY (influencer_id) REFERENCES influencers (id)');
        $this->addSql('ALTER TABLE richlist ADD CONSTRAINT FK_D7D726CE12469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE richlist ADD CONSTRAINT FK_D7D726CEF92F3E70 FOREIGN KEY (country_id) REFERENCES countries (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE richlist DROP FOREIGN KEY FK_D7D726CEF92F3E70');
        $this->addSql('ALTER TABLE richlist DROP FOREIGN KEY FK_D7D726CE4AF97FA6');
        $this->addSql('DROP TABLE countries');
        $this->addSql('DROP TABLE influencers');
        $this->addSql('DROP TABLE richlist');
    }
}
